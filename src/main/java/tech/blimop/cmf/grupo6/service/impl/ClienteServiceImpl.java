package tech.blimop.cmf.grupo6.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.blimop.cmf.grupo6.entity.Cliente;
import tech.blimop.cmf.grupo6.repository.ClienteRepository;
import tech.blimop.cmf.grupo6.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	@Autowired
	private ClienteRepository clienteRepository;	
	
	@Override
	public Cliente addCliente(Cliente cliente) {
		Cliente nuevoCliente = new Cliente();
		nuevoCliente.setId(cliente.getId());
		nuevoCliente.setDni(cliente.getDni());
		nuevoCliente.setNombre(cliente.getNombre());
		nuevoCliente.setTelefono(cliente.getTelefono());
		nuevoCliente.setEmail(cliente.getEmail());
		nuevoCliente.setDomicilio(cliente.getDomicilio());
		
		nuevoCliente = clienteRepository.save(nuevoCliente);
		return nuevoCliente;
	}

	@Override
	public Cliente updateCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public void deleteCliente(Long id) {
		clienteRepository.deleteById(id);
	}

}
