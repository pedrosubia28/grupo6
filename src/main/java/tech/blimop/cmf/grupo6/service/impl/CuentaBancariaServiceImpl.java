package tech.blimop.cmf.grupo6.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.blimop.cmf.grupo6.entity.CuentaBancaria;
import tech.blimop.cmf.grupo6.entity.Movimiento;
import tech.blimop.cmf.grupo6.repository.CuentaBancariaRepository;
import tech.blimop.cmf.grupo6.service.CuentaBancariaService;

@Service
public class CuentaBancariaServiceImpl implements CuentaBancariaService{
	@Autowired
	private CuentaBancariaRepository cuentaRepository;
	
	@Override
	public CuentaBancaria addCuenta(CuentaBancaria cuenta) {
		/*
		CuentaBancaria nuevaCuenta = new CuentaBancaria();
		nuevaCuenta.setId(cuentaBancaria.getId());
		nuevaCuenta.setCliente(cuentaBancaria.getCliente());
		nuevaCuenta.setNroCuenta(cuentaBancaria.getNroCuenta());
		nuevaCuenta.setFechaCreacion(cuentaBancaria.getFechaCreacion());
		nuevaCuenta.setSaldo(cuentaBancaria.getSaldo());
		nuevaCuenta = cuentaRepository.save(nuevaCuenta);
		*/
		cuentaRepository.save(cuenta);
		return cuenta;
	}
	
	@Override
	public void deleteCuenta(Long id) {
		cuentaRepository.deleteById(id);	
	}

	@Override
	public CuentaBancaria updateCuenta(CuentaBancaria cuenta) {
		return cuentaRepository.save(cuenta);
	}

	@Override
	public List<CuentaBancaria> getCuentasByDni(int dni) {
		List<CuentaBancaria> cuentas = cuentaRepository.findByDniCliente(dni);
		return cuentas;
	}

	@Override
	public List<Movimiento> getMovimientosByDni(int dni) {
		List<CuentaBancaria> cuentas = cuentaRepository.findByDniCliente(dni);
		List<Movimiento> movs = null;
		for (CuentaBancaria cuenta : cuentas) {
			List<Movimiento> auxMov = cuenta.getMovimientos();
			for (Movimiento mov: auxMov) {
				movs.add(mov);
			}
		}
		return movs;
	}

}
