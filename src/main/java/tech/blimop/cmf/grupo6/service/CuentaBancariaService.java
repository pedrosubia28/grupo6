package tech.blimop.cmf.grupo6.service;

import java.util.List;

import tech.blimop.cmf.grupo6.entity.CuentaBancaria;
import tech.blimop.cmf.grupo6.entity.Movimiento;

public interface CuentaBancariaService {

	public CuentaBancaria addCuenta(CuentaBancaria cuentaBancaria);
	
	public void deleteCuenta(Long id);
	
	public CuentaBancaria updateCuenta(CuentaBancaria cuentaBancaria);
	
	public List<CuentaBancaria> getCuentasByDni(int dni);
	
	public List<Movimiento> getMovimientosByDni(int dni);
}
