package tech.blimop.cmf.grupo6.service;

import tech.blimop.cmf.grupo6.entity.Cliente;

public interface ClienteService {

	Cliente addCliente(Cliente cliente);
	
	void deleteCliente(Long id);
	
	Cliente updateCliente(Cliente cliente);

}
