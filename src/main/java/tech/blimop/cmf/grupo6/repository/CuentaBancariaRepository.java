package tech.blimop.cmf.grupo6.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tech.blimop.cmf.grupo6.entity.CuentaBancaria;
@Repository
public interface CuentaBancariaRepository extends JpaRepository<CuentaBancaria, Long>{
	
	
	@Query(value="SELECT * FROM cuentas c JOIN clientes cl ON c.cliente_id = cl.id WHERE cl.dni= :dni",nativeQuery = true)
	List<CuentaBancaria> findByDniCliente(@Param("dni") int dni);
	
	//@Query(value="SELECT c FROM CuentaBancaria c JOIN clientes cl WHERE cl.dni = :dni")
	//List<CuentaBancaria> findByDniCliente(@Param("dni") int dni);
	
}
