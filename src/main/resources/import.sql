
insert into clientes (id, dni, telefono, nombre, email, domicilio) values(1, 999, 4200, 'cliente1', 'mail1@gmail.com', 'domicilio1');

insert into cuentas (id, cliente_id, nro_cuenta, fecha_creacion, saldo) values(1, 1, '111', '2022-02-02', 1000);

insert into movimientos (id, cuenta_bancaria_id, movimiento, importe, fecha) values (1, 1, 'DEPOSITO', 100, '2022-02-02');
