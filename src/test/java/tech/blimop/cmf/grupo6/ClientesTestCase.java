package tech.blimop.cmf.grupo6;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tech.blimop.cmf.grupo6.entity.Cliente;
import tech.blimop.cmf.grupo6.service.ClienteService;

@SpringBootTest
public class ClientesTestCase {
	@Autowired
	ClienteService clienteService;
	//static Cliente client;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
		//client = null;
	}

	@Test
	void testAgregarCliente() {
		Cliente client = new Cliente();
		client.setDni(111);
		client.setNombre("cliente2");
		client.setTelefono(4201);
		client.setEmail("email2@gmail.com");
		client.setDomicilio("domicilio2");
		
		
		Cliente clienteAgregado = clienteService.addCliente(client);
		assertThat(clienteAgregado).isNotNull();
		
		clienteService.deleteCliente(clienteAgregado.getId());
	}

}
