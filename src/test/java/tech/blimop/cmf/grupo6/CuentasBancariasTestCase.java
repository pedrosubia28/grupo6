package tech.blimop.cmf.grupo6;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tech.blimop.cmf.grupo6.entity.Cliente;
import tech.blimop.cmf.grupo6.entity.CuentaBancaria;
import tech.blimop.cmf.grupo6.service.ClienteService;
import tech.blimop.cmf.grupo6.service.CuentaBancariaService;

@SpringBootTest
public class CuentasBancariasTestCase {
	@Autowired
	CuentaBancariaService cuentaService;
	@Autowired
	ClienteService clienteServicio;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	@Test
	void testCrearCuenta() {
		
		Cliente clienteTest = new Cliente();

		clienteTest.setDni(222);
		clienteTest.setNombre("cliente3");
		clienteTest.setTelefono(4202);
		clienteTest.setEmail("email3@gmail.com");
		clienteTest.setDomicilio("domicilio3");
		
		clienteTest = clienteServicio.addCliente(clienteTest);
		
		CuentaBancaria cuentaTest = new CuentaBancaria();
		
		cuentaTest.setCliente(clienteTest);
		cuentaTest.setNroCuenta("222");
		cuentaTest.setFechaCreacion(LocalDate.now());
		cuentaTest.setSaldo(1000);
		
		CuentaBancaria cuentaNueva = cuentaService.addCuenta(cuentaTest);
		assertThat(cuentaNueva).isNotNull();
		
		cuentaService.deleteCuenta(cuentaTest.getId());
		clienteServicio.deleteCliente(clienteTest.getId());
		
	}
	
	@Test
	void testBuscarCuentaPorDni() {
		
		Cliente clienteTest = new Cliente();

		clienteTest.setDni(222);
		clienteTest.setNombre("cliente3");
		clienteTest.setTelefono(4202);
		clienteTest.setEmail("email3@gmail.com");
		clienteTest.setDomicilio("domicilio3");
		
		clienteTest = clienteServicio.addCliente(clienteTest);
		
		CuentaBancaria cuentaTest = new CuentaBancaria();
		
		cuentaTest.setCliente(clienteTest);
		cuentaTest.setNroCuenta("222");
		cuentaTest.setFechaCreacion(LocalDate.now());
		cuentaTest.setSaldo(1000);
		CuentaBancaria cuentaNueva = cuentaService.addCuenta(cuentaTest);
		
		List<CuentaBancaria> cuentas = cuentaService.getCuentasByDni(222);
		assertThat(cuentas).isNotEmpty();
		System.out.println("Array cuentas: " + cuentas.size());
		
		cuentaService.deleteCuenta(cuentaTest.getId());
		clienteServicio.deleteCliente(clienteTest.getId());
	}

}
